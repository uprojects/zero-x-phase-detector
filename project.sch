EESchema Schematic File Version 2
LIBS:kicad-spice
LIBS:project-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L R R1
U 1 1 54EBEC68
P 4100 3200
F 0 "R1" H 4100 3300 50  0000 C CNN
F 1 "560K" H 4100 3100 50  0000 C CNN
F 2 "" V 4030 3200 30  0000 C CNN
F 3 "" H 4100 3200 30  0000 C CNN
	1    4100 3200
	1    0    0    -1  
$EndComp
$Comp
L R R2
U 1 1 54EBEDD9
P 4100 4800
F 0 "R2" H 4100 4900 50  0000 C CNN
F 1 "560K" H 4100 4700 50  0000 C CNN
F 2 "" V 4030 4800 30  0000 C CNN
F 3 "" H 4100 4800 30  0000 C CNN
	1    4100 4800
	1    0    0    -1  
$EndComp
$Comp
L R R3
U 1 1 54EBEE0C
P 4900 4400
F 0 "R3" H 4900 4500 50  0000 C CNN
F 1 "330K" H 4900 4300 50  0000 C CNN
F 2 "" V 4830 4400 30  0000 C CNN
F 3 "" H 4900 4400 30  0000 C CNN
	1    4900 4400
	0    -1   -1   0   
$EndComp
$Comp
L R R4
U 1 1 54EBEF2C
P 5600 3600
F 0 "R4" H 5600 3700 50  0000 C CNN
F 1 "100" H 5600 3500 50  0000 C CNN
F 2 "" V 5530 3600 30  0000 C CNN
F 3 "" H 5600 3600 30  0000 C CNN
	1    5600 3600
	0    -1   -1   0   
$EndComp
$Comp
L C C1
U 1 1 54EBF021
P 5600 4400
F 0 "C1" H 5550 4550 50  0000 L CNN
F 1 "10n" H 5500 4250 50  0000 L CNN
F 2 "" H 5638 4250 30  0000 C CNN
F 3 "" H 5600 4400 60  0000 C CNN
	1    5600 4400
	0    -1   1    0   
$EndComp
$Comp
L QPNP Q1
U 1 1 54EBFD45
P 5500 3000
F 0 "Q1" H 5500 3200 60  0000 C CNN
F 1 "QSS9012" V 5750 3000 60  0000 C CNN
F 2 "" H 5500 3000 60  0000 C CNN
F 3 "" H 5500 3000 60  0000 C CNN
	1    5500 3000
	1    0    0    -1  
$EndComp
$Comp
L D XD1
U 1 1 54EC047D
P 4500 3600
F 0 "XD1" H 4500 3750 60  0000 C CNN
F 1 "1N4148" H 4500 3450 60  0000 C CNN
F 2 "" H 4500 3600 60  0000 C CNN
F 3 "" H 4500 3600 60  0000 C CNN
	1    4500 3600
	0    -1   1    0   
$EndComp
$Comp
L D XD3
U 1 1 54EC053D
P 5250 4000
F 0 "XD3" H 5250 4150 60  0000 C CNN
F 1 "1N4148" H 5250 3850 60  0000 C CNN
F 2 "" H 5250 4000 60  0000 C CNN
F 3 "" H 5250 4000 60  0000 C CNN
	1    5250 4000
	1    0    0    -1  
$EndComp
Text GLabel 6000 2700 2    60   Input ~ 0
qc
Text GLabel 4900 3000 0    60   Input ~ 0
qb
Text GLabel 5600 4000 2    60   Input ~ 0
cc
Text GLabel 5600 3300 2    60   Input ~ 0
qe
Text GLabel 4500 3200 2    60   Input ~ 0
da
Text GLabel 3700 3200 0    60   Input ~ 0
ac1
Text GLabel 3700 4800 0    60   Input ~ 0
ac0
$Comp
L V V2
U 1 1 54ECA53D
P 7500 3600
F 0 "V2" V 7300 3600 60  0000 C CNN
F 1 "DC 3.3V" V 7750 3600 60  0000 C CNN
F 2 "" H 7500 3600 60  0000 C CNN
F 3 "" H 7500 3600 60  0000 C CNN
	1    7500 3600
	1    0    0    -1  
$EndComp
$Comp
L R R7
U 1 1 55028E70
P 8000 3300
F 0 "R7" H 8000 3400 50  0000 C CNN
F 1 "10K" H 8000 3200 50  0000 C CNN
F 2 "" V 7930 3300 30  0000 C CNN
F 3 "" H 8000 3300 30  0000 C CNN
	1    8000 3300
	0    -1   -1   0   
$EndComp
$Comp
L R R8
U 1 1 55028EFA
P 8000 3900
F 0 "R8" H 8000 4000 50  0000 C CNN
F 1 "10K" H 8000 3800 50  0000 C CNN
F 2 "" V 7930 3900 30  0000 C CNN
F 3 "" H 8000 3900 30  0000 C CNN
	1    8000 3900
	0    -1   -1   0   
$EndComp
$Comp
L R R9
U 1 1 55028F35
P 8000 4500
F 0 "R9" H 8000 4600 50  0000 C CNN
F 1 "10K" H 8000 4400 50  0000 C CNN
F 2 "" V 7930 4500 30  0000 C CNN
F 3 "" H 8000 4500 30  0000 C CNN
	1    8000 4500
	0    -1   -1   0   
$EndComp
Text GLabel 8000 3600 2    60   Input ~ 0
l1
Text GLabel 8000 4200 2    60   Input ~ 0
l0
Text GLabel 7500 3000 1    60   Input ~ 0
vcc
$Comp
L OC XO1
U 1 1 550C629A
P 6200 4000
F 0 "XO1" H 6200 4200 60  0000 C CNN
F 1 "PC817C" H 6175 3800 60  0000 C CNN
F 2 "" H 5950 4000 60  0000 C CNN
F 3 "" H 5950 4000 60  0000 C CNN
	1    6200 4000
	1    0    0    -1  
$EndComp
$Comp
L DZ XD2
U 1 1 550C66F5
P 4500 4400
F 0 "XD2" H 4500 4525 60  0000 C CNN
F 1 "CMOZ18L" H 4500 4275 60  0000 C CNN
F 2 "" H 4500 4400 60  0000 C CNN
F 3 "" H 4500 4400 60  0000 C CNN
	1    4500 4400
	0    -1   -1   0   
$EndComp
$Comp
L V V1
U 1 1 550C7D63
P 3700 4000
F 0 "V1" V 3500 4000 60  0000 C CNN
F 1 "SIN(0V 311V 50Hz 0 0)" V 3925 4000 60  0000 C CNN
F 2 "" H 3375 4000 60  0000 C CNN
F 3 "" H 3375 4000 60  0000 C CNN
	1    3700 4000
	1    0    0    -1  
$EndComp
Text Notes 850  2800 0    60   ~ 0
-pspice\n* \n.SUBCKT 1N4148 1 2 \n*\n* The resistor R1 does not reflect \n* a physical device. Instead it\n* improves modeling in the reverse \n* mode of operation.\n*\nR1 1 2 5.827E+9 \nD1 1 2 1N4148\n*\n.MODEL 1N4148 D \n+ IS = 4.352E-9 \n+ N = 1.906 \n+ BV = 110 \n+ IBV = 0.0001 \n+ RS = 0.6458 \n+ CJO = 7.048E-13 \n+ VJ = 0.869 \n+ M = 0.03 \n+ FC = 0.5 \n+ TT = 3.48E-9 \n.ENDS
Text Notes 800  7550 0    60   ~ 0
-pspice\n* \n.MODEL QBC807_40 PNP\n+ IS = 2.077E-13 \n+ NF = 1.005 \n+ ISE = 1.411E-14 \n+ NE = 1.3 \n+ BF = 449.8 \n+ IKF = 0.36 \n+ VAF = 29 \n+ NR = 1.002 \n+ ISC = 2.963E-13 \n+ NC = 1.25 \n+ BR = 20.92 \n+ IKR = 0.104 \n+ VAR = 10 \n+ RB = 40 \n+ IRB = 1E-05 \n+ RBM = 5.3 \n+ RE = 0.14 \n+ RC = 0.32 \n+ XTB = 0 \n+ EG = 1.11 \n+ XTI = 3 \n+ CJE = 5E-11 \n+ VJE = 0.9296 \n+ MJE = 0.456 \n+ TF = 7E-10 \n+ XTF = 3.25 \n+ VTF = 2.5 \n+ ITF = 0.79 \n+ PTF = 80 \n+ CJC = 2.675E-11 \n+ VJC = 0.8956 \n+ MJC = 0.4638 \n+ XCJC =  0.459 \n+ TR = 3.5E-08 \n+ CJS = 0 \n+ VJS = 0.75 \n+ MJS = 0.333 \n+ FC = 0.935
Text Notes 2750 2450 0    60   ~ 0
-pspice\n* \n.SUBCKT CMOZ18L 1 2\n* Terminals    A   K\nD1 1 2 DF\nDZ 3 1 DR\nVZ 2 3 17.5\n.MODEL DF D\n+ IS=5.72p\n+ RS=19.7\n+ N=1.10\n+ CJO=41.5p\n+ VJ=1.00\n+ M=0.330\n+ TT=50.1n\n.MODEL DR D\n+ IS=1.14f\n+ RS=23.0\n+ N=0.743\n.ENDS
Text Notes 9800 4650 0    60   ~ 0
-pspice\n* \n.SUBCKT PC817C A K E C\nD1 A N001 LED\nQ1 C 5 E 0 NPN1 1\nR1 5 E 10G\nC1 A K 20p\nV1 N001 K 0\nR2 N002 0 1\nC2 N002 0 200n\nE1 0 N002 TABLE {I(V1)} =\n+ (0, 0)\n+ (10m, 10m)\n+ (15m, 14m)\n+ (20m, 17m)\n+ (40m, 25m)\n+ (80m, 35m)\n+ (160m, 50m)\nG1 E 5 N002 0 0.035\nC3 C 5 10p\nC4 A C 0.3p\nC5 K E 0.3p\nC6 5 E 10p\n.MODEL LED D\n+ Is=1e-15\n+ Rs=4\n+ N=1.5\n+ Eg=1.7\n+ CJO=30p\n+ TT=100n\n.MODEL NPN1 NPN\n+ Is=1e-12\n+ BF=200\n+ VAF=80\n+ IKF=0.025\n+ ISE=1e-9\n+ NE=2\n+ CJC=20p\n+ CJE=50p\n+ TF=5e-8\n+ Rb=100\n+ Rc=20\n+ Re=5\n.ENDS
Text Notes 2100 7550 0    60   ~ 0
-pspice\n* \n.MODEL QSS9012 PNP\n+is=2.0417e-14\n+bf=143.3\n+vaf=47.75\n+ikf=0.5743\n+ise=75.86f\n+ne=2\n+br=14.345\n+nr=0.999\n+var=86.14\n+ikr=0.4265\n+isc=1.585f\n+nc=1.0087\n+rb=57.5\n+irb=7.943u\n+rbm=8.09\n+re=0.02\n+rc=0.743\n+cje=35.1p\n+vje=0.866\n+mje=0.411\n+cjc=19.1p\n+vjc=0.787\n+mjc=0.394\n+xcjc=0.349\n+xtb=1.413\n+eg=1.0885\n+xti=3\n+fc=0.5\n+Vceo=20\n+Icrating=0.5
$Comp
L 0 #GND01
U 1 1 550C733F
P 5250 4900
F 0 "#GND01" H 5250 4800 40  0001 C CNN
F 1 "0" H 5250 4830 40  0000 C CNN
F 2 "" H 5250 4900 60  0000 C CNN
F 3 "" H 5250 4900 60  0000 C CNN
	1    5250 4900
	1    0    0    -1  
$EndComp
$Comp
L 0 #GND02
U 1 1 550C7393
P 7200 4900
F 0 "#GND02" H 7200 4800 40  0001 C CNN
F 1 "0" H 7200 4830 40  0000 C CNN
F 2 "" H 7200 4900 60  0000 C CNN
F 3 "" H 7200 4900 60  0000 C CNN
	1    7200 4900
	1    0    0    -1  
$EndComp
Text Notes 8450 3200 0    60   ~ 0
-pspice\n* \n.MODEL Q2N3904  NPN\n+ IS=4.9148E-15\n+ BF=191.70\n+ VAF=100\n+ IKF=.28579\n+ ISE=11.882E-15\n+ NE=1.4422\n+ BR=5.6808\n+ VAR=100\n+ IKR=61.753E-3\n+ ISC=71.145E-12\n+ NC=1.6595\n+ NK=.8296\n+ RB=5.8072\n+ RC=.70808\n+ CJE=6.9435E-12\n+ VJE=.61872\n+ MJE=.27802\n+ CJC=3.7572E-12\n+ VJC=1.2237\n+ MJC=.28886\n+ TF=523.89E-12\n+ XTF=83.066\n+ VTF=67.769\n+ ITF=1.8804\n+ TR=10.000E-9
$Comp
L R R5
U 1 1 550CD6C5
P 6400 3400
F 0 "R5" H 6400 3500 50  0000 C CNN
F 1 "10K" H 6400 3300 50  0000 C CNN
F 2 "" V 6330 3400 30  0000 C CNN
F 3 "" H 6400 3400 30  0000 C CNN
	1    6400 3400
	0    -1   -1   0   
$EndComp
Text GLabel 6400 3700 0    60   Input ~ 0
z1
Text Notes 4900 7200 0    60   ~ 0
+pspice\n* \n.control\n  option nopage\n  tran 10uS 200mS 0mS\n  set gnuplot_terminal=png\n  gnuplot project\n  + v(ac0,ac1)/100\n*  + i(V1)*v(ac0,ac1)*10\n*  + v(cc)\n  + v(qe,qb)\n*  + v(qe,qc)\n  + v(qc)\n  + v(l0) v(l1) v(zd)\n  let pwr=v(ac0,ac1)*i(V1)\n  meas tran pwrtest avg pwr\n  + from=0mS\n  + to=200mS\n.endc
Text GLabel 6400 4400 0    60   Input ~ 0
z0
$Comp
L R R6
U 1 1 550C9813
P 6900 3400
F 0 "R6" H 6900 3500 50  0000 C CNN
F 1 "10K" H 6900 3300 50  0000 C CNN
F 2 "" V 6830 3400 30  0000 C CNN
F 3 "" H 6900 3400 30  0000 C CNN
	1    6900 3400
	0    -1   -1   0   
$EndComp
$Comp
L QNPN Q2
U 1 1 550C9701
P 6800 4400
F 0 "Q2" H 6800 4600 60  0000 C CNN
F 1 "Q2N3904" V 7050 4400 60  0000 C CNN
F 2 "" H 6475 4400 60  0000 C CNN
F 3 "" H 6475 4400 60  0000 C CNN
	1    6800 4400
	1    0    0    -1  
$EndComp
Text GLabel 6900 3900 0    60   Input ~ 0
zd
Wire Wire Line
	4900 3000 5300 3000
Wire Wire Line
	4300 4800 6000 4800
Wire Wire Line
	4900 4600 4900 4800
Wire Wire Line
	4900 3000 4900 4200
Wire Wire Line
	5600 3400 5600 3200
Wire Wire Line
	5600 3800 5600 4300
Wire Wire Line
	5600 2800 5600 2700
Wire Wire Line
	5600 2700 6000 2700
Wire Wire Line
	5600 4500 5600 4800
Connection ~ 4900 4800
Wire Wire Line
	3700 3200 3700 3800
Wire Wire Line
	3700 4200 3700 4800
Wire Wire Line
	4500 4000 5150 4000
Connection ~ 4900 4000
Wire Wire Line
	5350 4000 5600 4000
Connection ~ 5600 4000
Wire Wire Line
	4500 3200 4500 3500
Wire Wire Line
	4300 3200 4500 3200
Wire Wire Line
	3900 3200 3700 3200
Wire Wire Line
	3700 4800 3900 4800
Wire Wire Line
	6000 2700 6000 3800
Wire Wire Line
	6000 4800 6000 4200
Connection ~ 5600 4800
Wire Wire Line
	7500 3000 7500 3400
Wire Wire Line
	7500 4800 7500 3800
Wire Wire Line
	6900 4800 8000 4800
Wire Wire Line
	6400 3000 8000 3000
Wire Wire Line
	8000 3000 8000 3100
Connection ~ 7500 3000
Wire Wire Line
	8000 4300 8000 4100
Wire Wire Line
	8000 3700 8000 3500
Wire Wire Line
	4500 4500 4500 4800
Connection ~ 4500 4800
Wire Wire Line
	5250 4800 5250 4900
Connection ~ 5250 4800
Wire Wire Line
	7200 4800 7200 4900
Wire Wire Line
	8000 4800 8000 4700
Connection ~ 7500 4800
Wire Wire Line
	6900 3600 6900 4200
Wire Wire Line
	6900 3200 6900 3000
Wire Wire Line
	6400 4200 6400 4400
Wire Wire Line
	6900 4600 6900 4800
Connection ~ 7200 4800
Wire Wire Line
	6400 3000 6400 3200
Connection ~ 6900 3000
Wire Wire Line
	6400 3600 6400 3800
Wire Wire Line
	4500 3700 4500 4300
Wire Wire Line
	6400 4400 6600 4400
Connection ~ 4500 4000
$EndSCHEMATC
